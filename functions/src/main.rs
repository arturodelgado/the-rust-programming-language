// Rust code uses snake case as the conventional style for
// function and variables names.
fn main() {
    print_labeled_measurement(5, 'h');

    // Expressions evaluate code
    // Calling a function is an expression.
    // Calling a macro is an expression.
    // A new scope block created with curly brackets is an expression
    let y: i32 = {
        // Statements do not return values
        let x: i32 = 3;
        x + 1
    };

    println!("The value of y is: {y}");

    let x: i32 = plus_one(5);
    println!("The value x is: {x}");
}

// Function arguments must be type declared
fn print_labeled_measurement(value: i32, unit_label: char) {
    println!("The measurement is: {value}{unit_label}");
}

// Functions can return values
// Return values are not named, but return type is declared (->)
// Return value is final expression but can return early by using `return`
fn plus_one(x: i32) -> i32 {
    x + 1
}
